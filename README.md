# Steps to initialize Sandbox

- Clone the repository recursively with submodules
- Switch to the `main` branch of every submodule
- Start the containers/services with `docker compose up --build -d`

# Debugging

- I've added launch.json settings in the .vscode directory which facilitates debug
- Go to the debug section in the activity bar and select whichever service you've to debug
- Select and start
- It'll atttach itself to the exposed debug port and debug right away

# Note

- Hot reloading is enabled for Express.js and Gin